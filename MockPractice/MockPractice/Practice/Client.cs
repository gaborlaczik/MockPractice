﻿using System;

namespace MockPractice.Practice
{
    public class Client : IDisposable
    {
		private IService Service { get; }
		private IContentFormatter ContentFormatter { get; }
		private int Identity { get; set; }

		public Client(IService service, IContentFormatter contentFormatter) // kiegészítettem null ellenörzéssel
        {
            if (service == null)
                throw new ArgumentNullException(nameof(service));

            if (contentFormatter == null)
                throw new ArgumentNullException(nameof(contentFormatter));

            Service = service;
			ContentFormatter = contentFormatter;
            Identity = -1; // 2 helyett inkább -1 lesz alapból, és a IService-t átírtam, hogy int-et adjon vissza a Connect, ami az id lesz majd mivel csatlakozáskor derül ki az ID
		}

        public string GetIdentity() // átírtam, hogy csatlakozzon ha még nem csatlakozott, mert amúgy honnan tudnánk az ID-t
        {
            if (!Service.IsConnected)
                Identity = Service.Connect();

            return Identity.ToString();
        }

        public string GetIdentityFormatted()
        {
            return $"<formatted> {GetIdentity()} </formatted>";
        }

        public string GetServiceName()
        {
            return Service.Name;
        }

        public void Dispose()
        {
			Service.Dispose();
        }

        public string GetContent(long id)
        {
            if(!Service.IsConnected)
            {
                Identity = Service.Connect();
            }
            
            var content = Service.GetContent(id);
            return content;
        }

		public string GetContentFormatted(long id)
		{
			var content = GetContent(id);
			var formattedContent = ContentFormatter.Format(content);
			return formattedContent;
		}
	}
}
