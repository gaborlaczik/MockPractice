﻿using System;

namespace MockPractice.Practice
{
    public interface IService : IDisposable
    {
        string Name { get; }
        bool IsConnected { get; }
        int Connect();
        string GetContent(long identity);
    }
}