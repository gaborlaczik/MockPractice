﻿using System.Linq;

namespace MockPractice.StringManipulator
{
	public class StringManipulator
	{
		private bool IsVowel(char c)
		{
            // https://simple.wikipedia.org/wiki/Vowel "sometimes Y.", így hozzá adtam az Y-t is
			var vowels = new char[] { 'a', 'e', 'i', 'o', 'u', 'y'};
			return vowels.Contains(char.ToLower(c)); // kiegészítettem a tolower-el
		}

		public string Transform(string s)
		{
			var lc = s.ToLower();
			var x = lc.Where(c => IsVowel(c));
			var y = lc.Where(c => !IsVowel(c));

			return new string(x.Concat(y).ToArray());
		}
	}
}
