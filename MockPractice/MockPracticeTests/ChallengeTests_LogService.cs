﻿using MockPractice.Challenge;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockPracticeTests
{
    [TestFixture]
    class ChallengeTests_LogService
    {
        [Test]
        public void Test_LogService_RegisterLogger_WithNull_ThrowsArgumentNullException()
        {
            // Arrange
            var logService = new LogService();

            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => logService.RegisterLogger(null));
        }

        [Test]
        public void Test_LogService_LogCallsMockLoggersLogAndSupporstLogLevelOnce()
        {
            // Arrange
            var mockLogger = new Mock<ILogger>();
            mockLogger.Setup(x => x.SupportsLogLevel(It.IsAny<LogLevel>())).Returns(true);

            var logService = new LogService();
            logService.RegisterLogger(mockLogger.Object);

            // Act
            logService.Log("test", LogLevel.Debug);

            // Assert
            mockLogger.Verify(x => x.Log(It.IsAny<string>(), It.IsAny<LogLevel>()), Times.Once);
            mockLogger.Verify(x => x.SupportsLogLevel(It.IsAny<LogLevel>()), Times.Once);
        }

        [TestCase(LogLevel.Debug, LogLevel.Debug, 1)]
        [TestCase(LogLevel.Info, LogLevel.Info, 1)]
        [TestCase(LogLevel.Error, LogLevel.Error, 1)]
        [TestCase(LogLevel.Error, LogLevel.Debug, 0)]
        [TestCase(LogLevel.Error | LogLevel.Debug, LogLevel.Debug, 1)]
        [TestCase(LogLevel.Error | LogLevel.Debug | LogLevel.Info, LogLevel.Debug, 1)]
        [TestCase(LogLevel.Error | LogLevel.Debug | LogLevel.Info, LogLevel.Error, 1)]
        [TestCase(LogLevel.Error | LogLevel.Debug | LogLevel.Info, LogLevel.Info, 1)]
        [TestCase(LogLevel.Error, LogLevel.Info, 0)]
        [TestCase(LogLevel.Debug, LogLevel.Info, 0)]
        [TestCase(LogLevel.Info, LogLevel.Info, 1)]
        public void Test_LogService_CallsGoodLogLevel_Debug(LogLevel loggerLevel, LogLevel logLogLevel, int callCount)
        {
            // Arrange
            var mockLogger = new Mock<ILogger>();
            mockLogger.Setup(x => x.SupportsLogLevel(It.IsAny<LogLevel>())).Returns(new Func<LogLevel, bool>(x => loggerLevel.HasFlag(logLogLevel)));

            var logService = new LogService();
            logService.RegisterLogger(mockLogger.Object);

            // Act
            logService.Log("test", logLogLevel);

            // Assert
            mockLogger.Verify(x => x.Log(It.IsAny<string>(), It.IsAny<LogLevel>()), Times.Exactly(callCount));
            mockLogger.Verify(x => x.SupportsLogLevel(It.IsAny<LogLevel>()), Times.Once());
        }

        public void Test_LogService_AssertResultFormat()
        {
            // Arrange
            var mockLogger = new Mock<ILogger>();
            mockLogger.Setup(x => x.SupportsLogLevel(It.IsAny<LogLevel>())).Returns(true);
            string resultMsg = null;
            LogLevel? resultLogLevel = null;
            mockLogger.Setup(x => x.Log(It.IsAny<string>(), It.IsAny<LogLevel>())).Callback(new Action<string, LogLevel>((msg, logLevel) =>
            {
                resultMsg = msg;
                resultLogLevel = logLevel;
            }));

            var logService = new LogService();
            logService.RegisterLogger(mockLogger.Object);

            // Act
            logService.Log("test", LogLevel.Info);

            // Assert
            Assert.IsTrue(resultLogLevel.HasValue);
            Assert.AreEqual(LogLevel.Info, resultLogLevel.Value);
            Assert.IsTrue(string.IsNullOrEmpty(resultMsg) == false);
            Assert.IsTrue(resultMsg.Split(new string[] { ": " }, StringSplitOptions.None).Length == 2);
        }
    }
}
