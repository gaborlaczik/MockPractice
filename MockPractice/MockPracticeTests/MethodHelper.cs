﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MockPracticeTests
{
    public static class MethodHelper
    {
        public static bool CallAnyInstanceMethod<T>(object obj, string methodName, out T result, params object[] args)
        {
            result = default(T);

            foreach(MethodInfo methodInfo in obj.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Default | BindingFlags.Instance | BindingFlags.Public).Where(x => x.IsStatic == false && x.ReturnType == typeof(T)))
            {
                if (methodInfo.Name == methodName)
                {
                    result = (T)methodInfo.Invoke(obj, args);
                    return true;
                }
            }

            return false;
        }
    }
}
