﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockPracticeTests
{
    class StringManipulatorTestCaseSource
    {
        public static IEnumerable<TestCaseData> tests
        {
            get
            {
                foreach(char c in "aeiouy")
                {
                    yield return new TestCaseData(c, true);
                    yield return new TestCaseData(char.ToUpper(c), true);
                }

                foreach (char c in "qwrtzpsdfghjklxcvbnm")
                {
                    yield return new TestCaseData(c, false);
                    yield return new TestCaseData(char.ToUpper(c), false);
                }
            }
        }
    }
}
