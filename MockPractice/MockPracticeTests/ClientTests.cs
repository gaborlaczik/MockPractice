﻿using MockPractice.Practice;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockPracticeTests
{
    [TestFixture]
    class ClientTests
    {
        #region Constructor
        [Test]
        public void Test_Constructor_ThrowsArgumentNullException_WhenServiceIsNull()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => new Client(null, mockFormatter.Object));
        }

        [Test]
        public void Test_Constructor_ThrowsArgumentNullException_WhenFormatterIsNull()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => new Client(mockService.Object, null));
        }

        [Test]
        public void Test_Constructor_ThrowsArgumentNullException_WhenServiceIsNullAndFormatterIsNull()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            // Act
            // Assert
            Assert.Throws<ArgumentNullException>(() => new Client(null, null));
        }

        [Test]
        public void Test_Constructor_ThrowsNothing_WhenParamtersAreGood()
        {
            // Arrange
            Exception throwedException = null;
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();

            // Act
            try
            {
                var client = new Client(mockService.Object, mockFormatter.Object);
            }
            catch(Exception ex)
            {
                throwedException = ex;
            }

            // Assert
            Assert.AreEqual(null, throwedException);
        }
        #endregion

        #region Identity
        [Test]
        public void Test_Client_GetIdentity_NotConnected_CallsConnectOnce()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            client.GetIdentity();

            // Assert
            mockService.Verify(x => x.Connect(), Times.Once);
        }

        [Test]
        public void Test_Client_GetIdentity_Connected_NotCallsConnect()
        {
            // Arrange
            var mockService = CreateMockService("test", true, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            client.GetIdentity();

            // Assert
            mockService.Verify(x => x.Connect(), Times.Never);
        }


        [Test]
        public void Test_Client_GetIdentity_ReturnsGoodIdentity()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            string result = client.GetIdentity();

            // Assert
            Assert.AreEqual(15.ToString(), result);
        }

        [Test]
        public void Test_Client_GetIdentityFormatted_NotConnected_CallsConnectOnce()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            client.GetIdentityFormatted();

            // Assert
            mockService.Verify(x => x.Connect(), Times.Once);
        }

        [Test]
        public void Test_Client_GetIdentityFormatted_Connected_NotCallsConnect()
        {
            // Arrange
            var mockService = CreateMockService("test", true, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            client.GetIdentityFormatted();

            // Assert
            mockService.Verify(x => x.Connect(), Times.Never);
        }

        [Test]
        public void Test_Client_GetIdentityFormatted_ReturnsGoodIdentity()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            string result = client.GetIdentityFormatted();

            // Assert
            Assert.AreEqual($"<formatted> 15 </formatted>".ToString(), result);
        }
        #endregion

        #region Dispose
        [Test]
        public void Test_CallClientDispose_CallsServiceDisposeOnce()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            client.Dispose();

            // Assert
            mockService.Verify(x => x.Dispose(), Times.Once);
        }
        #endregion

        #region ServiceName
        [Test]
        public void Test_ServiceName_ReturnsGoodName()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            string result = client.GetServiceName();

            // Assert
            Assert.AreEqual("test", result);
        }
        #endregion

        #region Content
        [Test]
        public void Test_GetContent_ServiceNotConnected_CallsConnectOnce()
        {
            // Arrange
            var mockService = CreateMockService("test", false, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            client.GetContent(0);

            // Assert
            mockService.Verify(x => x.Connect(), Times.Once);
        }

        [Test]
        public void Test_GetContent_ServiceConnected_CallsConnectNever()
        {
            // Arrange
            var mockService = CreateMockService("test", true, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            client.GetContent(0);

            // Assert
            mockService.Verify(x => x.Connect(), Times.Never);
        }


        [Test]
        public void Test_GetContent_ServiceConnected_ReturnsNonFormattedContent()
        {
            // Arrange
            var mockService = CreateMockService("test", true, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            string result = client.GetContent(100);

            // Assert
            Assert.AreEqual("content_100".ToString(), result);
        }

        [Test]
        public void Test_GetContentFormatted_ServiceConnected_CallsFormatterOnce()
        {
            // Arrange
            var mockService = CreateMockService("test", true, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            client.GetContentFormatted(100);

            // Assert
            mockFormatter.Verify(x => x.Format(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void Test_GetContentFormatted_ServiceConnected_ReturnsFormattedContent()
        {
            // Arrange
            var mockService = CreateMockService("test", true, "content_{0}", 15);
            var mockFormatter = CreateMockContentFormatter();
            Client client = new Client(mockService.Object, mockFormatter.Object);

            // Act
            string result = client.GetContentFormatted(100);

            // Assert
            Assert.AreEqual("@content_100@".ToString(), result);
        }
        #endregion


        #region Mock methods
        Mock<IService> CreateMockService(string name, bool connected, string contentFormat, int identity)
        {
            Mock<IService> mockService = new Mock<IService>();
            mockService.Setup(x => x.Name).Returns(() => name);
            mockService.Setup(x => x.IsConnected).Returns(() => connected);
            mockService.Setup(x => x.GetContent(It.IsAny<long>())).Returns(new Func<long, string>((id) => string.Format(contentFormat, id)));
            mockService.Setup(x => x.Connect()).Returns(identity);
            return mockService;
        }

        Mock<IContentFormatter> CreateMockContentFormatter()
        {
            Mock<IContentFormatter> mockContentFormatter = new Mock<IContentFormatter>();
            mockContentFormatter.Setup(x => x.Format(It.IsAny<string>())).Returns(new Func<string, string>((str) => $"@{str}@"));
            return mockContentFormatter;
        }
        #endregion
    }
}
