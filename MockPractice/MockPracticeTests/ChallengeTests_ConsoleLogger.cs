﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockPracticeTests
{
    [TestFixture]
    class ChallengeTests_ConsoleLogger
    {
        [TestCase(DayOfWeek.Friday)]
        [TestCase(DayOfWeek.Monday)]
        [TestCase(DayOfWeek.Saturday)]
        [TestCase(DayOfWeek.Sunday)]
        [TestCase(DayOfWeek.Thursday)]
        [TestCase(DayOfWeek.Tuesday)]
        [TestCase(DayOfWeek.Wednesday)]
        public void Test_ConsoleLogger_PrintNextWeekday_PrintsGoodDay(DayOfWeek dayOfWeek)
        {
            // ezt úgy csináltam meg, hogy nem wrappert írtam hanem a konzol kimenetét átirányítottam

            // Arrange
            StringWriter sw = new StringWriter();
            var defaultOut = Console.Out;
            Console.SetOut(sw);
            var logger = new MockPractice.Challenge.ConsoleLogger();

            // Act
            logger.PrintNextWeekday(dayOfWeek);
            sw.Flush();
            Console.SetOut(defaultOut);
            string result = sw.GetStringBuilder().ToString();

            // Assert
            DateTime resultDate = DateTime.Parse(result);
            Assert.IsTrue(resultDate.DayOfWeek == dayOfWeek);
            Assert.IsTrue(resultDate > DateTime.Now);
        }
    }
}
