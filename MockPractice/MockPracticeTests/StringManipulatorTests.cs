﻿using MockPractice.StringManipulator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockPracticeTests
{
    [TestFixture]
    class StringManipulatorTests
    {
        [TestCase(null, false)]
        [TestCaseSource(typeof(StringManipulatorTestCaseSource), nameof(StringManipulatorTestCaseSource.tests))]
        public void Test_IsVowel(char c, bool isVowel)
        {
            // Arrange
            StringManipulator stringManipulator = new StringManipulator();

            // Act
            bool result;

            if (!MethodHelper.CallAnyInstanceMethod<bool>(stringManipulator, "IsVowel", out result, c))
            {
                throw new Exception($"Problem with calling method 'IsVowel'");
            }

            // Assert
            Assert.AreEqual(isVowel, result);
        }
    }
}
